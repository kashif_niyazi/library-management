#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void member_area(user_t *u) {
	int choice;
	char name[80];
	do {
		printf("\n\n0. Sign Out\n1. Find Book\n2. Edit Profile\n3. Change Password\n4. Book Availability\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // Find Book
				printf("Enter book name: ");
				scanf("%s", name);
				book_find_by_name(name);
				break;
			case 2:
			edit_profile_id_member();
				break;
			case 3:
				break;
			case 4: // Book Availability
				bookcopy_checkavail();
				break;
		}
	}while (choice != 0);	
}

void edit_profile_id_member (){
	int id, found = 0;
	FILE *fp;
	user_t u;
	// input user id from user.
	printf("enter user id: ");
	scanf("%d", &id);
	// open users file
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open 0users file");
		exit(1);
	}
	// read users one by one and check if user with given id is found.
	while(fread(&u, sizeof(user_t), 1, fp) > 0) {
		if(id == u.id) {
			found = 1;
			break;
		}
	}
	// if found
	if(found) {
		// input new user details from user
		long size = sizeof(user_t);
		user_t nb;
		user_accept(&nb);
		nb.id = u.id;
		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite user details into the file
		fwrite(&nb, size, 1, fp);
		printf("profile updated.\n");
	}
	else // if not found
		// show message to user that profile not found.
		printf("profile not found.\n");
	// close users file
	fclose(fp);
		
}

void bookcopy_checkavail() {
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;
	// input book id
	printf("enter the book id: ");
	scanf("%d", &book_id);
	// open book copies file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open bookcopies file.");
		return;
	}

	// read bookcopy records one by one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// if book id is matching and status is available, count the copies
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL)==0) {
		//	bookcopy_display(&bc);
			count++;
		}
	}
	// close book copies file
	fclose(fp);
	// print the message. 
	printf("number of copies availables: %d\n", count);
}
