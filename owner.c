#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void owner_area(user_t *u) {
	int choice;
	do {
		printf("\n\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. Change Password\n4. Fees/Fine Report\n5. Book Availability\n6. Book Categories/Subjects\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // Appoint Librarian
				appoint_librarian();
				break;
			case 2:
		edit_profile_id_owner();
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				break;
			case 6:
				break;
		}
	}while (choice != 0);	
}

void edit_profile_id_owner (){
	int id, found = 0;
	FILE *fp;
	user_t u;
	// input user id from user.
	printf("enter user id: ");
	scanf("%d", &id);
	// open users file
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open 0users file");
		exit(1);
	}
	// read users one by one and check if user with given id is found.
	while(fread(&u, sizeof(user_t), 1, fp) > 0) {
		if(id == u.id) {
			found = 1;
			break;
		}
	}
	// if found
	if(found) {
		// input new user details from user
		long size = sizeof(user_t);
		user_t nb;
		user_accept(&nb);
		nb.id = u.id;
		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite user details into the file
		fwrite(&nb, size, 1, fp);
		printf("profile updated.\n");
	}
	else // if not found
		// show message to user that profile not found.
		printf("profile not found.\n");
	// close users file
	fclose(fp);
		
}

void appoint_librarian() {
	// input librarian details
	user_t u;
	user_accept(&u);
	// change user role to librarian
	strcpy(u.role, ROLE_LIBRARIAN);
	// add librarian into the users file
	user_add(&u);
}


